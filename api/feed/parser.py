# import datetime

# from django.db.models import Q
from django.utils import timezone
import feedparser

from feed.models import UserFeed, FeedItem


def fetch_feeds_task():
    is_data_fetched = False
    # TODO: try feed a RSS url for the firt time and skip urls newly updated
    now = timezone.datetime.now()
    # for uf in UserFeed.objects.filter(
    #         Q(fetch_date=None) |
    #         Q(fetch_date__gt=now-datetime.timedelta(minutes=5))):

    for uf in UserFeed.objects.all():
        # update fetch date
        uf.fetch_date = now
        uf.save()

        # fetching items from RSS
        news = feedparser.parse(uf.rss_url)
        if news.entries:
            is_data_fetched = True
            for entry in news.entries:
                try:
                    fi = FeedItem()
                    fi.userfeed = uf
                    fi.title = entry.title
                    fi.link = entry.link
                    fi.save()
                except Exception as e:
                    # TODO: logging this!
                    print(e)
    return is_data_fetched
