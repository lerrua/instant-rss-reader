from django.contrib import admin
from django.urls import include, path
from rest_framework import routers

from feed import views as feed_views


router = routers.DefaultRouter()
router.register(r'users', feed_views.UserViewSet)
router.register(r'feeds', feed_views.UserFeedViewSet)
router.register(r'news', feed_views.FeedItemViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('reload/', feed_views.FetchFeed.as_view()),
    path('admin/', admin.site.urls),
    path('api-auth/',
         include('rest_framework.urls', namespace='rest_framework'))
]
