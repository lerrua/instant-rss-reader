import uuid

from django.conf import settings
from django.db import models


class UserFeed(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='user_feed',
        on_delete=models.CASCADE
    )
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False
    )
    rss_url = models.CharField(max_length=255)
    fetch_date = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)


class FeedItem(models.Model):
    userfeed = models.ForeignKey(
        UserFeed,
        on_delete=models.CASCADE
    )
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False
    )
    link = models.CharField(max_length=255)
    content = models.TextField()
    is_read = models.BooleanField(default=False)
    publish_date = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ['userfeed', 'link']
