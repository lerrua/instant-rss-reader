from django.contrib.auth.models import User
from rest_framework import (
    views, viewsets, permissions, response, status)

from feed.models import FeedItem, UserFeed
from feed.parser import fetch_feeds_task
from feed.serializers import (
    FeedItemSerializer, UserFeedSerializer, UserSerializer)


class FetchFeed(views.APIView):
    """
    Manual endpoint to reload RSS feeds
    """
    def post(self, request, format=None):
        # invoke fetch RSS task
        # TODO: tranform this action into a parallalel job
        if fetch_feeds_task():
            return response.Response(data=None, status=status.HTTP_200_OK)
        return response.Response(data=None, status=status.HTTP_304_NOT_MODIFIED)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAdminUser]


class UserFeedViewSet(viewsets.ModelViewSet):
    queryset = UserFeed.objects.all()
    serializer_class = UserFeedSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or user.is_staff:
            queryset = UserFeed.objects.all().order_by('-created_at')
            return queryset
        queryset = UserFeed.objects.filter(user=user).order_by('-created_at')
        return queryset

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class FeedItemViewSet(viewsets.ModelViewSet):
    queryset = FeedItem.objects.filter(is_read=False).order_by('-created_at')
    serializer_class = FeedItemSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser or user.is_staff:
            queryset = FeedItem.objects.all().order_by('-created_at')
            return queryset
        queryset = FeedItem.objects.filter(user=user).order_by('-created_at')
        return queryset
