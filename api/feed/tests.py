from rest_framework import status
from rest_framework.test import APITestCase


class AccountTests(APITestCase):
    def test_users_list_fail(self):
        request = self.client.get('/users/', format='json')
        self.assertEqual(status.HTTP_403_FORBIDDEN, request.status_code)
