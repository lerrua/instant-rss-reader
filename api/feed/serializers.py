from django.contrib.auth.models import User
from rest_framework import serializers

from feed.models import UserFeed, FeedItem


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email']


class UserFeedSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.PrimaryKeyRelatedField(
        read_only=True, default=serializers.CurrentUserDefault())
    rss_url = serializers.URLField(
        max_length=255, min_length=None, allow_blank=False)

    class Meta:
        model = UserFeed
        fields = ['id', 'user', 'rss_url', 'fetch_date', 'created_at']


class FeedItemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FeedItem
        fields = ['id', 'userfeed', 'link', 'content', 'is_read',
                  'publish_date', 'created_at']
