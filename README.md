# Instant RSS Reader

## About

* An User should be able to create your personal reader 
* An enpoint to register the RSS feed from my favorite site (example: https://cointelegraph.com.br/rss)
* An endpoint to reload manually the data from a RSS feed url that I registered 
* List my  feed RSS with some options like pagination
* Flag an item as read

### How to run

```
git clone git@gitlab.com:lerrua/instant-rss-reader.git 
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
python api/manage.py migrate
```

Create an admin User with an password:
```
python manage.py createsuperuser --email admin@root.com --username root
```

## Goals

This is a simple app that aims to train my Python skills. I just went on vacation and stayed a while, so I challenged myself to deliver a complete API in just one day at work.

### TODOs:
- [x] Create an environment using Django REST framework
- [x] Create an endpoint to register new User accounts 
- [ ] Change the authentication method to use Tokens instead of Basic Auth 
- [x] Create an endpoint to register a new feed URL for my account 
- [x] Create an endpoint to push the manual reload service 
- [x] Create an endpoint to check my news item as read
- [x] Create an endpoint to check my news item as read
- [ ] Configure dev environment to run on docker-compose
- [ ] Improve coverage tests
- [ ] Create a service to run queue tasks and avoid overload the reload endpoint

## My daily status using pomodoro technique:

![alt text](production_status.png "production status")
