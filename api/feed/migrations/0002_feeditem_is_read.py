# Generated by Django 3.0.5 on 2020-04-06 22:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feed', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='feeditem',
            name='is_read',
            field=models.BooleanField(default=False),
        ),
    ]
